@extends('layouts.app')
@section('content')
      {{-- <h1>Index</h1>
      <p>{{$title}}</p> --}}

      <div class="jumbotron text-center">
        <h1>Welcome to Laravel</h1>
        <p>This is a laravel application</p>
        @if(Auth::guest())
          <p><a class="btn btn-primary btn-lag" href="/login" role="button">Login</a>
          <a class="btn btn-success btn-lag" href="/register" role="button">Register</a></p>
        @endif
@endsection
